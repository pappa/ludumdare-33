﻿using UnityEngine;
using System.Collections;

public class SnowTail : MonoBehaviour {

	//privates
	private GameObject[] particles;
	private bool isSpawning = false;
	//private bool isDestroying = false;
	private Transform currentPosition;

	//publics
	public GameObject particle;
	public float particleScale;
	public int numberOfParticles;
	public GameObject parent;

	delegate void TestDelegate(string s);
	// Use this for initialization
	void Start () 
	{
		particles = new GameObject[numberOfParticles];

		for(int i = 0; i < numberOfParticles; i++)
		{
			particles[i] = Instantiate(particle) as GameObject;
			particles[i].transform.localScale = new Vector3(particleScale, particleScale, particleScale);
			particles[i].SetActive(false);
		}
		Debug.Log("snow tail init");

	}// end Start();
	
	// Update is called once per frame
	void Update () 
	{
		currentPosition = parent.transform;

		if(!isSpawning)
			StartCoroutine(spawnParticle());
//		if (!isDestroying)
//			StartCoroutine (destroyParticle ());



	}
	// end Update();

	// start spawnParticle()
	private IEnumerator spawnParticle()
	{
		//float randomWait = Random.Range (.1f, .05f);
		isSpawning = true;

		yield return new WaitForSeconds(.07f);

		foreach(GameObject p in particles)
		{
			yield return new WaitForSeconds(.05f);

			if(p.activeSelf == false)
			{
				p.SetActive(true);
				Vector3 tempPos = currentPosition.position;
				tempPos.z = 2;

				p.transform.position = tempPos;
				p.GetComponent<ParticleTimer>().startFadeOut();
				break;
			}
		}
		isSpawning = false;

	}

	// start resetSnowTail()
	public void resetSnowTail()
	{
		isSpawning = false;
	}
	// end resetSnowTail();

}
