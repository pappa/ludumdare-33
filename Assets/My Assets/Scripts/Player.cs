﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	// publics
	public float moveSpeed;

	// privates
	private Rigidbody2D player;

	// Use this for initialization
	void Start () 
	{
		player = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		playerMovment();
	
	}

	// start playerMovment()
	private void playerMovment()
	{
		if(Input.GetKey(KeyCode.A))
		{
			player.velocity = Vector2.left * moveSpeed;
		
		}
		else if (Input.GetKey(KeyCode.D))
		{
			player.velocity = Vector2.right * moveSpeed;

		}
		else 
		{
			//GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
			player.velocity = new Vector2(0, player.velocity.y);
		}

	}

	// start OnTriggerEnter2D()
	private void OnTriggerEnter2D(Collider2D col)
	{
		if(col.gameObject.tag == "particle")
			col.gameObject.SetActive(false);

	} 
	// end OnTriggerEnter2D();
}
