﻿using UnityEngine;
using System.Collections;

public class FlakeSpawner : MonoBehaviour {

	//privates
	private GameObject[] flakes;
	private bool isSpawning;

	//publics 
	public int numberOfFlakes;
	public GameObject spawnObject;
	public float y;
	public float x;

	// Use this for initialization
	void Start () 
	{
		flakes = new GameObject[numberOfFlakes];

		for (int i = 0; i < numberOfFlakes; i++)
		{
			float size = Random.Range(.5f, .3f);

			flakes[i] = Instantiate(spawnObject) as GameObject;
			flakes[i].SetActive(false);
			flakes[i].transform.localScale = new Vector3(size, size, size);
			//Debug.Log(flakes[i]);
		}
	}
	
	// Update is called once per frame
	void Update () 
	{	
		if(!isSpawning)
		{
			StartCoroutine(waitForFlake());
		}
	}

	// start waitForFlake()
	private IEnumerator waitForFlake()
	{
		float randomWait = Random.Range(1f,.5f);
		isSpawning = true;

		yield return new WaitForSeconds(randomWait);

		foreach(GameObject flake in flakes)
		{
			if(flake.activeSelf == false)
			{
				flake.SetActive(true);
				flake.transform.position = new Vector2(Random.Range(-6, 6),y);
				break;
			}

		}
		isSpawning = false;


	} 
	// end waitForFlake();
}
