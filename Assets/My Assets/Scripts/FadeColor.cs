﻿using UnityEngine;
using System.Collections;

public class FadeColor : MonoBehaviour {

	private Color originalColor;
	private Color spriteColor;

	public float defaultAlpha = 1;

	private GameObject _oObject;
	public GameObject ParentObject
	{
		get { return _oObject; }
		set 
		{ 
			_oObject = value;
			originalColor = value.GetComponent<SpriteRenderer>().color;
			spriteColor = originalColor;

		}
	}

	private float _fadeAmount;
	public float FadeAmount
	{
		get { return _fadeAmount; }
		set { _fadeAmount = value; }
	}

	private float _waitTime;
	public float WaitTime
	{
		get { return _waitTime; }
		set { _waitTime = value; }
	}


	public void startFadeOut()
	{ 
		spriteColor.a = defaultAlpha;

		ParentObject.GetComponent<SpriteRenderer>().color = originalColor;
		StartCoroutine(fadeOut(WaitTime));
	}

	public void startFadeIn()
	{ StartCoroutine(fadeIn(WaitTime)); }

	IEnumerator fadeOut(float waitAmount)
	{
		while(spriteColor.a > 0.1f)
		{
			spriteColor.a -= FadeAmount;
			ParentObject.GetComponent<SpriteRenderer>().color = spriteColor;
			yield return new WaitForSeconds(waitAmount);
		}

		// i dont think this is a good idea || BUT MAYBE
		ParentObject.SetActive(false);

	}

	IEnumerator fadeIn(float waitAmount)
	{
		ParentObject.SetActive(true);

		while(spriteColor.a <= originalColor.a)
		{
			spriteColor.a += FadeAmount;
			ParentObject.GetComponent<SpriteRenderer>().color = spriteColor;
			
			yield return new WaitForSeconds(waitAmount);
		}
	}


}
