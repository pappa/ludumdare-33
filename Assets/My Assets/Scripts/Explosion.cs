﻿using UnityEngine;
//using System;
using System.Collections;
using System.Collections.Generic;

public class Explosion {

	// privates
	private GameObject[] particles;
	private Dictionary<int, Vector2> projectileTrajectorys = new Dictionary<int, Vector2>();
	private float xSpeedIncrement;
	private Vector3 firstTrajectory;

	private float _projectileSpeed;
	public float ProjectileSpeed
	{
		get {return _projectileSpeed; }
		set 
		{ _projectileSpeed = value; }
	}

	private GameObject _projectile;
	public GameObject Projectile
	{
		get { return _projectile; }
		set 
		{ 
			if(value.GetComponent<Rigidbody2D>() != null)
			{ }
			else 
			{
				Debug.Log("Adding rigidbody");
				value.AddComponent<Rigidbody2D>();
			}

			_projectile = value;
		}
	}

	private int _numberOfProjectiles;
	public int NumberOfProjectiles
	{

		get {return _numberOfProjectiles; }
		set
		{
			_numberOfProjectiles = value; 
			particles = new GameObject[value];

			for(int i = 0; i < value; i++)
			{
				particles[i] = Object.Instantiate(Projectile);
				particles[i].SetActive(false);
			}
		}

	}

	// publics
	public Vector3 parentPosition;
	public float projectileSize;

	// start trajectoryInstantiator();
	/// <summary>
	/// Make sure to run this after you have set all of your values!
	/// </summary>
	public void trajectoryInstantiator()
	{
		
		xSpeedIncrement = (ProjectileSpeed * 2) / NumberOfProjectiles;
		float subtractY = 0;
		bool startSubstract = true;
		//Debug.Log(xSpeedIncrement);

		for(int i = 0; i < NumberOfProjectiles; i++)
		{
			float x = ProjectileSpeed - (i * xSpeedIncrement);
			float y = 0;

			if(i >= (NumberOfProjectiles / 2))
			{
				if(startSubstract)
				{
					subtractY = xSpeedIncrement * i;
					startSubstract = false;
				}
				else
				{

					y = subtractY - xSpeedIncrement;
					subtractY = y;
				}
			}
			else
			{
				y = xSpeedIncrement * i;
				//Debug.Log("add y " + y + " increment " + i);
			}

			projectileTrajectorys.Add (i, new Vector2(x, y));
			//Debug.Log("yspeed " + y  + " increment " + i);
		}
	} 
	// end trajectoryInstantiator();

	// start 	Projectiles()
	/// <summary>
	/// Spawns the projectiles.
	/// </summary>
	public void spawnProjectiles()
	{
		Vector3 shit = new Vector3 (0, 0, 0);
		for(int i = 0; i < NumberOfProjectiles; i++)	
		{
			//Debug.Log (projectileTrajectorys[i]);
			particles[i].transform.localScale = new Vector3(projectileSize, projectileSize, projectileSize);
			particles[i].transform.position = parentPosition;

			//Debug.Log ( particles[i].activeSelf ? "object is active" : "object is not active");

			particles[i].SetActive(true);

			//Debug.Log ( particles[i].activeSelf ? "object is active" : "object is not active");

			//if(firstTrajectory == null)
			Debug.Log ((firstTrajectory == shit ? "firstTrajectory is empty" : "firstTrajectory is set" + firstTrajectory));

			particles[i].GetComponent<Rigidbody2D>().AddForce(projectileTrajectorys[i]);
			particles[i].GetComponent<ParticleTimer>().startFadeOut();
			//fader.startFadeOut();
		}
		//Debug.Log ("setting particles to active");
	} 
	// end spawnProjectiles();


	private void testStyff()
	{
		Debug.Log(ProjectileSpeed + " teststuff");
	} 
	// end projectileTrajectory();
}
