﻿using UnityEngine;
using System.Collections;

public class Flake : MonoBehaviour {

	// privates
	private SnowTail fSpawner;
	private Explosion boom = new Explosion();

	// publics
	public float speed;
	public int numberOfProjectiles = 1;
	public float projectileSize;
	public float projectileSpeed;

	// Use this for initialization
	void Start ()
	{
		//boom = new Explosion();
		fSpawner = gameObject.GetComponent<SnowTail>();

		boom.projectileSize = projectileSize;
		boom.ProjectileSpeed = projectileSpeed;
		boom.Projectile = Resources.Load("Projectile") as GameObject;
		boom.NumberOfProjectiles = numberOfProjectiles;		
		boom.trajectoryInstantiator();
	}

	// Update is called once per frame
	void Update () 
	{
		transform.Translate(Vector2.up * -(Time.deltaTime * speed), Space.World);
		rotateFlake(Random.Range(60,80));

	}

	// start rotateFlake()
	private void rotateFlake(float speed)
	{
		transform.Rotate(0,0, Time.deltaTime * speed);

	}
	// end rotateFlake();

	// start OnTriggerEnter2D()
	private void OnTriggerEnter2D(Collider2D col)
	{
		 fSpawner.resetSnowTail();

		// depending on what the flake hits boom.Projectile is set to diferent prefabs	
		if(col.gameObject.tag == "floor")
		{
			//boom.Projectile = Resources.Load("SnowProjectile") as GameObject;
			//boom.NumberOfProjectiles = numberOfProjectiles;
			boom.parentPosition = gameObject.transform.position;

			boom.spawnProjectiles();

			gameObject.SetActive(false);	
		
		}
		else if(col.gameObject.tag == "player")
		{
			//boom.Projectile = Resources.Load("Projectile") as GameObject;
			//boom.NumberOfProjectiles = numberOfProjectiles;			
			boom.parentPosition = gameObject.transform.position;

			boom.spawnProjectiles();

			gameObject.SetActive(false);
		}

	}
	// end OnTriggerEnter2D();

	// 


}
